# Load required packages
library(ggplot2)
library(readr)
library(xtable)
library(reshape2)
library(RColorBrewer)
library(patchwork)

# Read the data
data <- read_csv("scripts/results/ablation.csv")

# Baselines
baseline <- c(
    "ddi_minerals" = 0.987,
    "ddi_efficacy" = 0.975,
    "dep_fda_exp" = 0.304,
    "dpi_fda" = 0.742
)
data$hits_at_10_filtered <- mapply(function(x, y) x-y, data$hits_at_10_filtered, baseline[as.character(data$benchmark)])

# Rename the benchmarks
data$benchmark <- sub("ddi_minerals", sprintf("DDI-Minerals (%s)", baseline["ddi_minerals"]), data$benchmark)
data$benchmark <- sub("ddi_efficacy", sprintf("DDI-Minerals (%s)", baseline["ddi_efficacy"]), data$benchmark)
data$benchmark <- sub("dep_fda_exp", sprintf("DEP-FDA-EXP (%s)", baseline["dep_fda_exp"]), data$benchmark)
data$benchmark <- sub("dpi_fda", sprintf("DPI-FDA (%s)", baseline["dpi_fda"]), data$benchmark)

data$hits_at_10_filtered <- round(data$hits_at_10_filtered, digits = 3)

# Colors
color_palette <- c(brewer.pal(9, "Set1"), brewer.pal(8, "Set2"))
old_first <- color_palette[1]
color_palette[1] <- color_palette[6]
color_palette[6] <- old_first


# Convert the 'model' variable to a factor
data$model <- as.factor(paste(data$model, " "))

# Create a new variable 'model_num' that represents the numeric version of 'model'
data$model_num <- as.factor(as.numeric(data$model))

ggplot(data, aes(y = hits_at_10_filtered, x = benchmark, label = model)) +
    geom_bar(aes(fill = model), position = position_dodge(width = 0.8), width = 0.7, stat = "identity") +
    scale_fill_manual(values = setNames(color_palette, levels(data$model)), name = "Removed Relation") +
    theme_minimal() +
    labs(x = NULL, y = "Hits@10") +
    theme(text = element_text(size = 14))
ggsave("relation_ablation.pdf", width = 12, height = 6)

# Reshape the data to get hits_at_10_filtered values for each combination of benchmark and model
reshaped_data <- t(dcast(data, benchmark ~ model, value.var = "hits_at_10_filtered"))

# Convert the reshaped data to a LaTeX table
latex_table <- xtable(reshaped_data)

# Print the LaTeX table
print(latex_table, type = "latex")
