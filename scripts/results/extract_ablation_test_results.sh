#!/bin/bash

# Make output file
touch results.csv
echo "benchmark, model, hits_at_1, hits_at_3, hits_at_10, hits_at_1_filtered, hits_at_3_filtered, hits_at_10_filtered, mrr, mrr_filtered" > results.csv

# Relation map
declare -A relation_map
relation_map[0]="COMPLEX_IN_PATHWAY"
relation_map[1]="COMPLEX_TOP_LEVEL_PATHWAY"
relation_map[2]="DDI"
relation_map[3]="DISEASE_GENETIC_DISORDER"
relation_map[4]="DISEASE_PATHWAY_ASSOCIATION"
relation_map[5]="DPI"
relation_map[6]="DRUG_CARRIER"
relation_map[7]="DRUG_DISEASE_ASSOCIATION"
relation_map[8]="DRUG_ENZYME"
relation_map[9]="DRUG_PATHWAY_ASSOCIATION"
relation_map[10]="DRUG_TARGET"
relation_map[11]="DRUG_TRANSPORTER"
relation_map[12]="MEMBER_OF_COMPLEX"
relation_map[13]="PPI"
relation_map[14]="PROTEIN_DISEASE_ASSOCIATION"
relation_map[15]="PROTEIN_PATHWAY_ASSOCIATION"
relation_map[16]="RELATED_GENETIC_DISORDER"

# Loop over all directories
for benchmark in $(ls -d *); do
    echo $benchmark
    for dir in $(ls -d $benchmark/*); do
        echo $dir
        #LD_LIBRARY_PATH=/home/$USER/.conda/envs/kge_playground/lib/python3.10/site-packages/nvidia/cublas/lib/ kge test $dir > $dir/out.txt
        hits_at_1=$(grep -oP 'hits_at_1: \K[0-9]*\.?[0-9]+' $dir/out.txt)
        hits_at_3=$(grep -oP 'hits_at_3: \K[0-9]*\.?[0-9]+' $dir/out.txt)
        hits_at_10=$(grep -oP 'hits_at_10: \K[0-9]*\.?[0-9]+' $dir/out.txt)
        hits_at_1_filtered=$(grep -oP 'hits_at_1_filtered: \K[0-9]*\.?[0-9]+' $dir/out.txt)
        hits_at_3_filtered=$(grep -oP 'hits_at_3_filtered: \K[0-9]*\.?[0-9]+' $dir/out.txt)
        hits_at_10_filtered=$(grep -oP 'hits_at_10_filtered: \K[0-9]*\.?[0-9]+' $dir/out.txt)
        mrr=$(grep -oP 'mean_reciprocal_rank: \K[0-9]*\.?[0-9]+' $dir/out.txt)
        mrr_filtered=$(grep -oP 'mean_reciprocal_rank_filtered: \K[0-9]*\.?[0-9]+' $dir/out.txt)

        relation_id=$(echo $dir | grep -oP '\d+')
        echo $relation_id
        relation_name=${relation_map[$relation_id]}
        echo "$benchmark, $relation_name, $hits_at_1, $hits_at_3, $hits_at_10, $hits_at_1_filtered, $hits_at_3_filtered, $hits_at_10_filtered, $mrr, $mrr_filtered" >> results.csv
    done
done